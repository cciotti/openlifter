# OpenLifter

> Simple software for running a local powerlifting competition.

#### Development Setup

``` bash
# Serves with hot reload at localhost:9080.
make
```

#### Build Setup

``` bash
# Install dependencies.
yarn

# Serve with hot reload at localhost:9080.
yarn run dev

# Build electron application for production.
yarn run build

# Run unit & end-to-end tests.
yarn test

# Lint all JS/Vue component files in `src/`.
yarn run lint

```
