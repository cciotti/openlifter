.PHONY: run

all: run

node_modules:
	yarn

run: node_modules
	yarn run dev
