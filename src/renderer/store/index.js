import Vue from 'vue'
import Vuex from 'vuex'

// FIXME: Importation of vuex-electron breaks 'npm run build:web'.
// FIXME: Also uncomment the plugins code below.
// import { createPersistedState, createSharedMutations } from 'vuex-electron'

import modules from './modules'

Vue.use(Vuex)

export default new Vuex.Store({
  modules,
  plugins: [
    // createPersistedState(),
    // createSharedMutations()
  ],
  strict: process.env.NODE_ENV !== 'production'
})
